package com.project.restfulapispringsecurity.Service;

import com.project.restfulapispringsecurity.Exception.BadRequestException;
import com.project.restfulapispringsecurity.Repository.Student;
import com.project.restfulapispringsecurity.Repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImplementation implements StudentService {

    @Autowired
    StudentRepository studentRepository;

    @Override
    public Student save(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public List<Student> findAll() {
        return  studentRepository.findAll();
    }

    @Override
    public Student findById(long id) {
        return studentRepository.findById(id).orElseThrow(() -> new BadRequestException("Student does not exist"));
    }

    @Override
    public void deleteById(long id) {
        studentRepository.deleteById(id);
    }

    @Override
    public Student updateById(long id, Student student) {
        student.setId(id);
        return studentRepository.save(student);
    }
}
