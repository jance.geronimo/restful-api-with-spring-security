package com.project.restfulapispringsecurity;

import com.project.restfulapispringsecurity.Repository.Student;
import com.project.restfulapispringsecurity.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class StudentData implements CommandLineRunner {
    @Autowired
    private StudentService studentService;

    private final ArrayList<Student> defaultStudentData = new ArrayList<>();

    @Override
    public void run(String... args) throws Exception {
        populateStudentTable();
    }

    private void populateStudentTable() {
        Student student1 = new Student();
        student1.setFirstName("Tyler");
        student1.setLastName("Durden");
        student1.setCourse("Cinematography");
        student1.setEmail("tylerdurden@gmail.com");
        student1.setGPA(3.78);
        studentService.save(student1);

        Student student2 = new Student();
        student2.setFirstName("Patrick");
        student2.setLastName("Bateman");
        student2.setCourse("Directing");
        student2.setEmail("patrickbateman@gmail.com");
        student2.setGPA(2.92);
        studentService.save(student2);

        Student student3 = new Student();
        student3.setFirstName("Travis");
        student3.setLastName("Bickle");
        student3.setCourse("Introduction to Filmmaking");
        student3.setEmail("travisbickle@yahoo.com");
        student3.setGPA(3.65);
        studentService.save(student3);

        Student student4 = new Student();
        student4.setFirstName("Walter");
        student4.setLastName("White");
        student4.setCourse("Screenwriting");
        student4.setEmail("heisenberg@outlook.com");
        student4.setGPA(3.21);
        studentService.save(student4);

        Student student5 = new Student();
        student5.setFirstName("Donnie");
        student5.setLastName("Darko");
        student5.setCourse("Film Editing");
        student5.setEmail("donniedarko@aol.com");
        student5.setGPA(3.95);
        studentService.save(student5);

        Student student6 = new Student();
        student6.setFirstName("Kentaro");
        student6.setLastName("Miura");
        student6.setCourse("Sound Design");
        student6.setEmail("kentaromiura@protonmail.com");
        student6.setGPA(2.87);
        studentService.save(student6);

        Student student7 = new Student();
        student7.setFirstName("Johan");
        student7.setLastName("Libert");
        student7.setCourse("Film Theory and Analysis");
        student7.setEmail("monster@icloud.com");
        student7.setGPA(3.45);
        studentService.save(student7);

        Student student8 = new Student();
        student8.setFirstName("Musashi");
        student8.setLastName("Miyamoto");
        student8.setCourse("Directing");
        student8.setEmail("musashimiyamoto@zoho.com");
        student8.setGPA(3.12);
        studentService.save(student8);

        Student student9 = new Student();
        student9.setFirstName("Thorfinn");
        student9.setLastName("Karlsefni");
        student9.setCourse("Production Design");
        student9.setEmail("thorfinn@mail.com");
        student9.setGPA(3.78);
        studentService.save(student9);

        Student student10 = new Student();
        student10.setFirstName("Spike");
        student10.setLastName("Spiegel");
        student10.setCourse("Cinematography");
        student10.setEmail("spikespiegel@gmx.com");
        student10.setGPA(3.55);
        studentService.save(student10);

        defaultStudentData.addAll(studentService.findAll());
    }

}

