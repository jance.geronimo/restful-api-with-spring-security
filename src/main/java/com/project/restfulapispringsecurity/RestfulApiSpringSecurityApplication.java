package com.project.restfulapispringsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestfulApiSpringSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestfulApiSpringSecurityApplication.class, args);
	}

}
