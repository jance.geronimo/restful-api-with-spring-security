package com.project.restfulapispringsecurity.Controller;

import com.project.restfulapispringsecurity.Repository.Student;
import com.project.restfulapispringsecurity.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @PostMapping("/addStudent")
    public Student addStudent(@RequestBody Student student){
        return studentService.save(student);
    }

    @GetMapping("/getAllStudents")
    public List<Student> getAllStudents(){
        return studentService.findAll();
    }

    @GetMapping("/getById/{id}")
    public Student getById(@PathVariable long id){
        return studentService.findById(id);
    }

    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@PathVariable long id){
        studentService.deleteById(id);
    }

    @PutMapping("/updateById/{id}")
    public Student updateById(@PathVariable long id, @RequestBody Student student){
        return studentService.updateById(id, student);
    }

}

